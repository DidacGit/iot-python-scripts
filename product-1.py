# -*- coding: utf-8 -*-
import ssl
import sys
import paho.mqtt.client
import time
from multiprocessing import Process
from random import randint

# This program simulates the hotel room.
# It uses MQTT clients to connect to the server.
# It also uses Python's multiprocessing library for async procedures.

broker='127.0.0.1'
port=1883

# Called when the client is connected
def on_connect(client, userdata, flags, rc):
    topic='hall/#'
    client.subscribe(topic, 1)
    print('Subscribed to: %s' % topic)
    print('------------------------------')

# Called when the client receives a message from a subscription
def on_message(client, userdata, message):
    print('topic: %s' % message.topic)
    print('payload: %s' % message.payload)
    print('------------------------------')

# Async function that creates and connects the SBC client
def sbc_function():
    sbc = paho.mqtt.client.Client(client_id='sbc', clean_session=False)
    sbc.on_connect = on_connect
    sbc.on_message = on_message
    sbc.connect(broker, port)
    sbc.loop_forever()

# Async function that creates and connects the door sensor client
def hall_door_sensor_function():
    hallDoorSensor = paho.mqtt.client.Client(client_id='doorSensor', clean_session=False)
    hallDoorSensor.connect(broker, port)
    while True:
        # Every 2..6 seconds, the door opens
        time.sleep(randint(2,6))
        hallDoorSensor.publish("hall/door/sensor", "Door Opened")

# Async function that creates and connects the hall temperature sensor client
def hall_temp_sensor_function():
    hallTempSensor = paho.mqtt.client.Client(client_id='tempSensor', clean_session=False)
    hallTempSensor.connect(broker, port)
    while True:
        # Every second, the temperature may change
        time.sleep(1)
        temp=randint(19,24)
        hallTempSensor.publish("hall/temp/sensor", "Temperature: %d°C" % temp)

def main():
    # Invoke the server process, it represents the SBC that subscribes to the sensors
    sbcProcess = Process(target=sbc_function)
    sbcProcess.start()
    # Invoke the two sensors processes
    hallDoorSensorProcess = Process(target=hall_door_sensor_function)
    hallDoorSensorProcess.start()
    hallTempSensorProcess = Process(target=hall_temp_sensor_function)
    hallTempSensorProcess.start()
    
if __name__ == '__main__':
    main()
 
sys.exit(0)
