import random
import time
from azure.iot.device import IoTHubDeviceClient, Message

# This programs connects to the Azure Hub and sends the data about the hotel room.
# In this case it simulates a temperature and humidity sensor of the room.
# It uses the Azure IoT library.

# The device connection string to authenticate the device with your IoT hub.
# Obtained with (using the Azure shell):
# az iot hub device-identity show-connection-string --hub-name {YourIoTHubName} --device-id MyNodeDevice --output table
# Connection string of the machine.
CONNECTION_STRING = "ENTER_STRING_HERE"

# Define the JSON message to send to IoT Hub.
TEMPERATURE = 20.0
HUMIDITY = 60
MSG_TXT = '{{"temperature": {temperature},"humidity": {humidity}}}'

# Create an IoT Hub client.
def iothub_client_init():
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    return client

# Procedure with the main loop sending the data.
def iothub_client_telemetry_run():

    try:
        client = iothub_client_init()
        print ( "IoT Hub device sending periodic messages, press Ctrl-C to exit" )

        while True:
            # Build the message with simulated telemetry values.
            temperature = TEMPERATURE + (random.random() * 15)
            humidity = HUMIDITY + (random.random() * 20)
            msg_txt_formatted = MSG_TXT.format(temperature=temperature, humidity=humidity)
            message = Message(msg_txt_formatted)

            # Add a custom application property to the message.
            # An IoT hub can filter on these properties without access to the message body.
            if temperature > 30:
              message.custom_properties["temperatureAlert"] = "true"
            else:
              message.custom_properties["temperatureAlert"] = "false"

            # Send the message.
            print( "Sending message: {}".format(message) )
            client.send_message(message)
            print ( "Message successfully sent" )
            time.sleep(1)

    except KeyboardInterrupt:
        print ( "IoTHubClient stopped" )

# Main program.
if __name__ == '__main__':
    print ( "IoT Hub - Simulated device" )
    print ( "Press Ctrl-C to exit" )
    iothub_client_telemetry_run()
